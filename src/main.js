import { createApp } from 'vue'
import App from './App.vue'
import '@/assets/style/index.scss'

const app = createApp(App)
import pinia from '@/store/index.js'
import router from '@/router/index.js'
import i18n from '@/i18n/index.js'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import lang from 'element-plus/lib/locale/lang/zh-cn'

for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(ElementPlus, {
  locale: lang,
})
app.use(pinia)
app.use(router)
app.use(i18n)
app.mount('#app')
