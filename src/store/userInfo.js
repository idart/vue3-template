import { defineStore } from 'pinia'

// 第一个参数是应用程序中 store 的唯一 id
export const userInfo = defineStore('userInfo', {
  state: () => ({
    userInfo: {
      name: 'lisi',
      age: 18,
    },
  }),
  actions: {
    async setUserInfo(data) {
      this.userInfo = data
    },
  },
})
