import request from '@/utils/request'

export function testRequestFun() {
  return request({
    url: `/homePage/getProcessNode`,
    method: 'get',
  })
}
