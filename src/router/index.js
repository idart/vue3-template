// 1.从vue-router导出两个方法使用
import { createRouter, createWebHashHistory, createWebHistory } from 'vue-router'
import Layout from '@/layout/index.vue'

// 2.声明菜单数组
const routes = [
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    title: '首页',
    children: [
      {
        path: 'home',
        title: '首页',
        component: () => import('../view/home/index.vue'),
      },
      {
        path: 'about',
        title: '关于我',
        component: () => import('../view/about/index.vue'),
      },
    ],
  },
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const routerHistory = createWebHashHistory(process.env.BASE_URL)
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: routerHistory,
  routes, // `routes: routes` 的缩写
})

// 导出路由
export default router
