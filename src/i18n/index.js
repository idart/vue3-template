import { createI18n } from 'vue-i18n'
import homeEn from './home/en.js'
import homeZh from './home/zh.js'

const messages = {
  en: {
    home: { ...homeEn },
  },
  zh: {
    home: { ...homeZh },
  },
}

const language = (navigator.language || 'zh').toLocaleLowerCase() // 这是获取浏览器的语言

const i18n = createI18n({
  legacy: false,
  locale: localStorage.getItem('i18n') || language.split('-')[0] || 'zh', // 首先从缓存里拿，没有的话就用浏览器语言，
  fallbackLocale: 'zh', // 设置备用语言
  messages,
})

export default i18n
